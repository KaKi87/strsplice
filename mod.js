/**
 * @param   {String|{string: String, start: Number, deleteCount: Number|undefined, item: String|undefined}} arg
 * @param   {Number} [start]
 * @param   {Number} [deleteCount]
 * @param   {String} [item]
 * @returns {String}
 */
const stringSplice = (
    arg,
    start,
    deleteCount,
    item
) => {
    const
        _string = (typeof arg === 'string' ? arg : arg.string).split(''),
        _start = typeof arg.start === 'number' ? arg.start : start,
        _deleteCount = typeof arg.deleteCount === 'number' ? arg.deleteCount : deleteCount,
        _item = typeof arg.item === 'string' ? arg.item : item;
    _string.splice(_start, _deleteCount, _item);
    return _string.join('');
};

/**
 * @param   {String} string
 * @param   {{start: Number, deleteCount: Number|undefined, item: String|undefined}[]} items
 * @returns {String}
 */
const stringSpliceMulti = (string, items) => {
    let n = 0;
    for(
        const { start, deleteCount, item }
        of items.sort((a, b) => a.start - b.start)
    ){
        string = stringSplice(string, start + n, deleteCount, item);
        n += item?.length || 0;
        n -= deleteCount  || 0;
    }
    return string;
};

export {
    stringSplice,
    stringSpliceMulti
};